
import React,{useState} from 'react';
import './NewExpense.css'
import NewForm from './NewForm';

const NewExpense = (props) =>{

    const [isEditing , setIsEditing ] = useState(false);
    const addExpenseDataHandler = (enteredexpenseData) => {
        const expenseData =  {
            ...enteredexpenseData,
            id: Math.random().toString()
        };
        props.onAddExpenseData(expenseData);
        setIsEditing(false);
    }

    const startEditingHandler = () =>{
        setIsEditing(true);
    }
    
    const stopEditingHandler =()=>{
        setIsEditing(false)
    }


    return(
       <div className='new-expense'>
          {!isEditing && <button onClick={startEditingHandler}>Add Expense</button>}
           { isEditing && <NewForm onSaveExpenseData={addExpenseDataHandler} onCancle={stopEditingHandler}/>}
       </div>
    );
};


export default NewExpense;