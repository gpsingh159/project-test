import React, {
    useState
} from 'react';
import './NewForm.css'
const NewForm = (props) => {

    // const [enteredTitle, setEnteredTitle]  =  useState('');
    // const [enteredAmount, setEnteredAmount]  =  useState('');
    // const [enteredDate, setEnteredDate]  =  useState('');

    // const [userInput, setUserInput] = useState({
    //     enteredTitle: '',
    //     enteredAmount: '',
    //     enteredDate: ''

    // })
    // const titleChangeHandler = (event) => {
    //     //    setEnteredTitle(event.target.value);
    //     setUserInput({
    //         ...userInput,
    //         enteredTitle: event.target.value
    //     })

    //     //console.log(event.target.name);
    //     //  console.log(event.target.value);
    // }


    // const amountChangeHandler = (event) => {
    //     setUserInput({
    //         ...userInput,
    //         enteredAmount:event.target.value
    //     });
    //   //  setEnteredAmount(event.target.value);
    // }

    // const dateChangeHandler = (event) => {
    //     //setEnteredDate(event.target.value);
    //     setUserInput({
    //         ...userInput,
    //         enteredDate: event.target.value
    //     });
    // }

    /// 2 way

    const inputParams = {title:'',amount:'',date:''} ;

    const [userInput , setUserInput] = useState(inputParams);

    const inputChangeHandler =(event) =>{
        const name = event.target.name ;
        const value= event.target.value;
        setUserInput( (prevState) =>
             {return {...prevState, [name]:value }}
             );
      //  console.log(userInput);
    }
    const submitHandle = (event) => {
        event.preventDefault();
        // console.log(enteredTitle);
        // console.log(enteredAmount);
        // console.log(enteredDate);
        // setEnteredTitle("");
        // setEnteredAmount("");
        // setEnteredDate("");
        //------------------------
        // console.log(userInput);
        // setUserInput({
        //     ...userInput,
        //     enteredTitle: '',
        //     enteredAmount: '',
        //     enteredDate: ''
        // });
        //-----------
        //2nd way
        userInput['date'] = new Date(userInput['date']);
        props.onSaveExpenseData(userInput)
     
        setUserInput({
            ...userInput,
            title: '', amount: '', date: ''
        });
        


    };

    const CancleHandler = ()=>{
        props.onCancle();
    }

    return(
        // <form onSubmit={submitHandle}>
        //     <div className="new-expense__controls">
        //         <div className="new-expense__control">
        //             <label>Title</label>
        //             <input type='text' value={userInput.enteredTitle} name="title" onChange={titleChangeHandler} />
        //         </div>
        //         <div className="new-expense__control">
        //             <label>Amount</label>
        //             <input type='number' value={userInput.enteredAmount}  min="0.01" step="0.01"   onChange={amountChangeHandler}  />
        //         </div>
        //         <div className="new-expense__control">
        //             <label>Date</label>
        //             <input type='date'  min="2019-01-01" max="2022-12-01" value={userInput.enteredDate}  onChange={dateChangeHandler}  />
        //         </div>
        //         <div className='new-expense__control'>
        //             <button type='submit' >Add Expenses</button>
        //         </div>
        //     </div>
        // </form>
        // s2nd way 

        <form onSubmit={submitHandle}>
            <div className="new-expense__controls">
                <div className="new-expense__control">
                    <label>Title</label>
                    <input type='text' name="title" value={userInput.title}  onChange={inputChangeHandler} />
                </div>
                <div className="new-expense__control">
                    <label>Amount</label>
                    <input type='number' name="amount" value={userInput.amount}  min="0.01" step="0.01"   onChange={inputChangeHandler}  />
                </div>
                <div className="new-expense__control">
                    <label>Date</label>
                    <input type='date'  min="2019-01-01" name="date" max="2022-12-01" value={userInput.date}  onChange={inputChangeHandler}  />
                </div>
                <div className='new-expense__actions'>
                <button type='button' onClick={CancleHandler} >Cancle</button>
                    <button type='submit' >Add Expenses</button>
                </div>
            </div>
        </form>
    );
};


export default NewForm;