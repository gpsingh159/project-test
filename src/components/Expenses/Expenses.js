import React, { useState } from "react";
import "./Expenses.css";
// import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "./ExpensesChart";

const Expenses = (props) => {
  // let filteredExpenses = [] ;

  // for(const exp of props.items){
  //     filteredExpenses.push(
  //         <ExpenseItem  key ={exp.id}
  //         title = {exp.title}
  //         amount = {exp.amount}
  //         date = {exp.date}

  //         />
  //     )
  //}

  // const filteredExpenses =  props.items.map((expense) => <ExpenseItem
  //             key ={expense.id}
  //             title = {expense.title}
  //             amount = {expense.amount}
  //             date = {expense.date} />);

  // maintain the state
  const [filteredYear, setFilteredYear] = useState("2020");

  const filterChangeHandler = (selectedValue) => {
    setFilteredYear(selectedValue);
  };

  const filteredExpenses = props.items.filter((FilterExpenses) => {
    return FilterExpenses.date.getFullYear().toString() === filteredYear;
  });

  return (
    <div>
      <Card className="expenses">
        <ExpensesFilter
          selected={filteredYear}
          onChangeFilter={filterChangeHandler}
        />
        <ExpensesChart expenses={filteredExpenses} />
        <ExpensesList items={filteredExpenses} />
      </Card>
    </div>
  );
};

export default Expenses;
