import "./ExpenseItem.css";
import ExpenseDate from "./ExpenseDate";
import Card from "../UI/Card";
// import React, { useState } from "react";
// const ExpenseDate = new Date().toISOString().slice(0, 10);

const ExpenseItem = (props) => {
  // const [title ,setTitle] = useState(props.title);
  // const clickHandle = (props) => {
  //     setTitle("updated");
  //     console.log(title);

  // };

  return (
    <li>
      <Card className="expense-item">
        <ExpenseDate date={props.date} />
        <div className="expense-item__description">
          {/* <h2>{title}</h2> */}
          <h2>{props.title}</h2>
          <div className="expense-item__price">{props.amount}</div>
          {/* <button onClick={clickHandle}>Update</button> */}
        </div>
      </Card>
    </li>
  );
};

export default ExpenseItem;
